#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <HCSR04.h>

const char *ssid =  "1st-floor";
const char *pass =  "121628784357";

#define TRIG D1
#define ECHO D2
#define MOTOR_CONTROL_PIN D4
UltraSonicDistanceSensor distanceSensor(TRIG,ECHO);

float distanceCm;

ESP8266WebServer server(80);

void handleLevelRequest(){
  float distanceInch = 0.393701*distanceSensor.measureDistanceCm();
  server.send(200,"text",String(distanceInch));
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  server.send(404, "text/plain", message);
}

void handleStatus(){
  if(digitalRead(MOTOR_CONTROL_PIN)==0)//MOTOR ON
    server.send(200, "text/plain","on"); 
  else server.send(200, "text/plain","off");
}

void handleMotorSwitch () {
  String motorStatus = server.arg("motorStatus");
  Serial.println (motorStatus);
  if (motorStatus == "on") {
    Serial.println ("Server wants to turn on the motor");
    digitalWrite(MOTOR_CONTROL_PIN,LOW);//Relay is active LOW
    server.send(200, "text/plain","Motor Turned On"); 
  } else {    
    Serial.println ("Server wants to turn off the motor");
    digitalWrite(MOTOR_CONTROL_PIN,HIGH);//Relay is active LOW
    server.send(200, "text/plain","Motor Turned Off");
  }
}

void measure_Volume()
{
  distanceCm=distanceSensor.measureDistanceCm();
  Serial.print("Distance (cm)=");
  Serial.println(distanceCm);
  if(digitalRead(MOTOR_CONTROL_PIN)==0) {
    Serial.println("Motor is On");
  } else {
    Serial.println("Motor is Off");
  }   
}

void runPeriodicFunc()
{
  static const unsigned long REFRESH_INTERVAL1 = 2100; // 2.1sec
  static unsigned long lastRefreshTime1 = 0;
  
  if(millis() - lastRefreshTime1 >= REFRESH_INTERVAL1)
  {   
    measure_Volume();
    lastRefreshTime1 = millis();
  }
}

void setup() 
{
  Serial.begin(115200);
  delay(100);
  pinMode(MOTOR_CONTROL_PIN, OUTPUT);
  digitalWrite(MOTOR_CONTROL_PIN,HIGH);         
  Serial.println("Connecting to ");
  Serial.println(ssid); 
  
  WiFi.begin(ssid, pass); 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected"); 
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());   //You can get IP address assigned to ESP
  Serial.print("MAC: ");
  Serial.println(WiFi.macAddress());

  server.on("/level",handleLevelRequest);
  server.on("/motor_status",handleStatus);
  server.on("/motor_switch",handleMotorSwitch);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}
 
void loop() 
{      
  runPeriodicFunc();
  server.handleClient();
}
