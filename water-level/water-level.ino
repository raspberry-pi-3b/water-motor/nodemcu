#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <HCSR04.h>

const char *ssid =  "1st-floor";
const char *pass =  "121628784357";

#define LENGTH 72
#define WIDTH 72
#define MAX_HEIGHT 2
#define TRIG D1
#define ECHO D2
#define MOTOR_CONTROL_PIN D4
UltraSonicDistanceSensor distanceSensor(TRIG,ECHO);

int waterLevelLowerThreshold=3;
int waterLevelUpperThreshold=12;

float distanceCm;
float volume;
float liters;

String inputString = "";         // a string to hold incoming data
String dataToSend="";
int waterLevelDownCount=0, waterLevelUpCount=0;

ESP8266WebServer server(80);

void handleLevelRequest(){
  server.send(200,"text",String(liters));
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  server.send(404, "text/plain", message);
}

void handleStatus(){
  if(digitalRead(MOTOR_CONTROL_PIN)==0)//MOTOR ON
    server.send(200, "text/plain","on"); 
  else server.send(200, "text/plain","off");
}

void handleRangeSetting(){
  waterLevelLowerThreshold=(server.arg(0)).toInt();  
  waterLevelUpperThreshold=(server.arg(1)).toInt(); 
  Serial.print(waterLevelLowerThreshold);
  Serial.print(":");   
  Serial.println(waterLevelUpperThreshold);
  
  server.send(200, "text/plain", "");
}

void measure_Volume()
{
  distanceCm=distanceSensor.measureDistanceCm();
  Serial.print("Distance (cm)=");
  Serial.println(distanceCm);  
  float heightInch=0.393701*distanceCm;    
  Serial.println(heightInch); 
  if(heightInch>MAX_HEIGHT)
    heightInch=MAX_HEIGHT;
  if(heightInch<0)
    heightInch=0;      
  volume=LENGTH*WIDTH*(MAX_HEIGHT-heightInch);//MAX_HEIGHT-distance will give actual height,
  liters=volume*0.0163871;
  Serial.println(liters); 

  if(liters<=waterLevelLowerThreshold)
    waterLevelDownCount++;
  else waterLevelDownCount=0;

  if(liters>=waterLevelUpperThreshold)
    waterLevelUpCount++;
  else waterLevelUpCount=0;  
  

  if(waterLevelDownCount==3)
  {//TURN ON RELAY
    Serial.println("motor turned on");   
    digitalWrite(MOTOR_CONTROL_PIN,LOW);//Relay is active LOW
  }
  if(waterLevelUpCount==3)
  {//TURN OFF RELAY
    Serial.println("motor turned off");   
    digitalWrite(MOTOR_CONTROL_PIN,HIGH);//Relay is active LOW
  }      
}

void runPeriodicFunc()
{
  static const unsigned long REFRESH_INTERVAL1 = 2100; // 2.1sec
  static unsigned long lastRefreshTime1 = 0;
  
  if(millis() - lastRefreshTime1 >= REFRESH_INTERVAL1)
  {   
    measure_Volume();
    lastRefreshTime1 = millis();
  }
}

void setup() 
{
  Serial.begin(115200);
  delay(100);
  pinMode(MOTOR_CONTROL_PIN, OUTPUT);
         
  Serial.println("Connecting to ");
  Serial.println(ssid); 
  
  WiFi.begin(ssid, pass); 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected"); 
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());   //You can get IP address assigned to ESP
  Serial.print("MAC: ");
  Serial.println(WiFi.macAddress());

  server.on("/level",handleLevelRequest);
  server.on("/configRange",handleRangeSetting);
  server.on("/motor_status",handleStatus);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}
 
void loop() 
{      
  runPeriodicFunc();
  server.handleClient();
}
