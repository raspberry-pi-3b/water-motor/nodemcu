#define trigPin D4          //  const int trigPin = 2;      //D4    Input for sensor  Output for Node
#define echoPin D3          //  const int echoPin = 0;      //D3    Output for sensor Input for Node
#define motorPin D5         //  const int motorPin = 14;    //D5    Input for relay Output for Node


// defines variables
long duration;
int distance;
int count;
bool isOn;

const int onThresh = 30;    //DISTANCE AT WHICH YOU WANT TO SWITCH THE MOTOR ON OR OFF
const int countThresh = 5;  //COUNT DOWN
const int delayTime = 1000;

void setup() {
pinMode(trigPin, OUTPUT);   // Sets the trigPin as an Output
pinMode(motorPin, OUTPUT);  // Sets the trigPin as an Output
pinMode(echoPin, INPUT);    // Sets the echoPin as an Input

Serial.begin(9600);         // Starts the serial communication
count= 0;
isOn= true;
digitalWrite(motorPin, HIGH);
}

void loop() {
// Clears the trigPin
digitalWrite(trigPin, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);


// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);

// Calculating the distance
distance = duration * 0.034 / 2; 
// Prints the distance on the Serial Monitor
Serial.print("Distance: ");
Serial.println(distance);
if(isOn)
{
  digitalWrite(motorPin, HIGH);
  if (distance < onThresh)
  {
    if (count < countThresh)
    {
      Serial.print("Count(off): ");
      Serial.println(count);
      count++;
    }
    else
    {
      Serial.println("TURN OFF");
      count= 0;
      isOn= false;
      digitalWrite(motorPin, LOW);
    }
  }
}
else
{
  digitalWrite(motorPin, LOW);
  if (distance >= onThresh)
  {
    if (count < countThresh)
    {
      Serial.print("Count(on): ");
      Serial.println(count);
      count++;
    }
    else
    {
      Serial.println("TURN ON");
      count= 0;
      isOn= true;
      digitalWrite(motorPin, HIGH);
    }
  }
}
delay(delayTime);
}

