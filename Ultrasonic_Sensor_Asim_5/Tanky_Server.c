//LIBRARIES
#include <arpa/inet.h>
#include <ctype.h>
#include <limits.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#define clear() printf("\033[H\033[J")


//CONSTANTS
#define message_size 2048
#define maxIPSize 15
#define SLEEPTIME 3
#define totalProv 4
#define totalSubProv 8


#define FED "127.0.0.1"		//FEDERAL IP

#define B "127.0.0.1"			//BALOCHISTAN IP
#define K "127.0.0.1"			//KPK IP
#define P "127.0.0.1"			//PUNJAB IP
#define S "127.0.0.1"			//SINDH IP


#define BL "127.0.0.1"			//BALOCHISTAN LOWER IP
#define BU "127.0.0.1"			//BALOCHISTAN UPPER IP
#define KL "127.0.0.1"			//KPK LOWER IP
#define KU "127.0.0.1"			//KPK UPPER IP
#define PL "127.0.0.1"			//PUNJAB LOWER IP
#define PU "127.0.0.1"			//PUNJAB UPPER IP
#define SL "127.0.0.1"			//SINDH LOWER IP
#define SU "127.0.0.1"			//SINDH UPPER IP

#define PORTFED 5050			//FEDERAL PORT

#define PORTB 5051			//BALOCHISTAN PORT
#define PORTK 5054			//KPK PORT
#define PORTP 5057			//PUNJAB PORT
#define PORTS 5060			//SINDH PORT


#define PORTBL 5053			//BALOCHISTAN LOWER PORT
#define PORTBU 5052			//BALOCHISTAN UPPER PORT
#define PORTKL 5056			//BALOCHISTAN LOWER PORT
#define PORTKU 5055			//BALOCHISTAN UPPER PORT
#define PORTPL 5059			//BALOCHISTAN LOWER PORT
#define PORTPU 5058			//BALOCHISTAN UPPER PORT
#define PORTSL 5062			//BALOCHISTAN LOWER PORT
#define PORTSU 5061			//BALOCHISTAN UPPER PORT

typedef struct Province
{
	int bindID ;
	int myPort ;
	int sockID ;
	int threadID ;
	char myIP [ maxIPSize ] ;
	pthread_t thread ;
	struct sockaddr_in myServer ;
} Province ;

typedef struct Client
{
	char msg [ message_size ] ;
	int clientID ;
	int clientThreadID ;
	int masterPort ;
	pthread_t clientThread ;
	struct sockaddr_in clientAddr ;
} Client ;

//GLOBALS
int voteCountPMLN [ totalProv ] [ ( totalSubProv / totalProv ) ] ;
int voteCountPTI [ totalProv ] [ ( totalSubProv / totalProv ) ] ;


//PROTOTYPE
int checkSlot ( pthread_mutex_t mut , int* cSlots ) ;
void* provThread ( void* temp ) ;
void* voteThread ( void* temp ) ;

//FUNCTIONS
int main ( int argc , char* argv[] )
{
	system ( "tput reset" ) ;
	printf ( "\t\tINITIALIZING PROVINCIAL SERVERS\n" ) ;
	int first = 50 ;
  int second = 7 ;
  int third = 28 ;
	int provPorts [ totalProv ] = { PORTB , PORTK , PORTP , PORTS } ;
	char provIP [ totalProv ] [ maxIPSize ] = { B , K , P , S } ;
	for ( int i = 0 ; i < totalProv ; i++ )
	{
		for ( int j = 0 ; j < ( ( totalSubProv / totalProv ) ) ; j++ )
		{
			voteCountPMLN [ i ] [ j ] = 0 ;
			voteCountPTI  [ i ] [ j ] = 0 ;
		}
	}

	struct Province provList [ totalProv ] ;
	for ( int i = 0 ; i < totalProv ; i++ )
	{
		provList [ i ].myPort = provPorts [ i ] ;
		strcpy ( provList [ i ].myIP , provIP [ i ] ) ;
		provList [ i ].threadID = pthread_create ( & ( provList [ i ].thread ) , NULL , provThread , ( void* ) &provList [ i ] ) ;
	}
	sleep ( SLEEPTIME ) ;
	while ( 1 )
	{
		system ( "tput reset" ) ;
    printf("%*s%s\n", first/2 , "", "PROVINCIAL SCOREBOARD");
    printf("-------------------------------------------------------------\n");

    printf ( "%*s%s%*s%s\n" , first/2 , "" , "PMLN"  , first/2 , "" , "PTI "  ) ;
    printf ( "BALOCHISTAN: LO  = %*s%d%*s%d\n             \
UP  = %*s%d%*s%d\n             \
TOT = %*s%d%*s%d\n-------------------------------------------------------------\n\
KPK:         LO  = %*s%d%*s%d\n             \
UP  = %*s%d%*s%d\n             \
TOT = %*s%d%*s%d\n-------------------------------------------------------------\n\
PUNJAB:      LO  = %*s%d%*s%d\n             \
UP  = %*s%d%*s%d\n             \
TOT = %*s%d%*s%d\n-------------------------------------------------------------\n\
SINDH :      LO  = %*s%d%*s%d\n             \
UP  = %*s%d%*s%d\n             \
TOT = %*s%d%*s%d\n-------------------------------------------------------------\n" ,  second , "" , voteCountPMLN [ 0 ] [ 0 ] , third , "" , voteCountPTI [ 0 ] [ 0 ] ,
                         second , "" , voteCountPMLN [ 0 ] [ 1 ] , third , "" , voteCountPTI [ 0 ] [ 1 ] ,
                         second , "" , ( voteCountPMLN [ 0 ] [ 0 ] + voteCountPMLN [ 0 ] [ 1 ] ) , third , "" , ( voteCountPTI [ 0 ] [ 0 ] + voteCountPTI [ 0 ] [ 1 ] ) ,
                         second , "" , voteCountPMLN [ 1 ] [ 0 ] , third , "" , voteCountPTI [ 1 ] [ 0 ] ,
                         second , "" , voteCountPMLN [ 1 ] [ 1 ] , third , "" , voteCountPTI [ 1 ] [ 1 ] ,
                         second , "" , ( voteCountPMLN [ 1 ] [ 0 ] + voteCountPMLN [ 1 ] [ 1 ] ) , third , "" , ( voteCountPTI [ 1 ] [ 0 ] + voteCountPTI [ 1 ] [ 1 ] ) ,
                         second , "" , voteCountPMLN [ 2 ] [ 0 ] , third , "" , voteCountPTI [ 2 ] [ 0 ] ,
                         second , "" , voteCountPMLN [ 2 ] [ 1 ] , third , "" , voteCountPTI [ 2 ] [ 1 ] ,
                         second , "" , ( voteCountPMLN [ 2 ] [ 0 ] + voteCountPMLN [ 2 ] [ 1 ] ) , third , "" , ( voteCountPTI [ 2 ] [ 0 ] + voteCountPTI [ 2 ] [ 1 ] ) ,
                         second , "" , voteCountPMLN [ 3 ] [ 0 ] , third , "" , voteCountPTI [ 3 ] [ 0 ] ,
                         second , "" , voteCountPMLN [ 3 ] [ 1 ] , third , "" , voteCountPTI [ 3 ] [ 1 ] ,
                         second , "" , ( voteCountPMLN [ 3 ] [ 0 ] + voteCountPMLN [ 3 ] [ 1 ] ) , third , "" , ( voteCountPTI [ 3 ] [ 0 ] + voteCountPTI [ 3 ] [ 1 ] )
             ) ;
						 int PMLNTOTAL = 0 ;
						 int PTITOTAL = 0 ;

						 for ( int i = 0 ; i < totalProv ; i++ )
						 {
							 for ( int j = 0 ; j < ( totalSubProv / totalProv ) ; j++ )
							 {
								 PMLNTOTAL += voteCountPMLN [ i ] [ j ] ;
								 PTITOTAL += voteCountPTI [ i ] [ j ] ;

							 }
						 }
						 printf ( "\n\n\n\nPMLN TOTAL = %d\n\n" , PMLNTOTAL ) ;
						 printf ( "PTI  TOTAL = %d\n\n" , PTITOTAL ) ;
    sleep ( SLEEPTIME ) ;
	}
}

void* provThread ( void* provVar )
{
	struct Province provServer = *( (struct Province*) provVar );
	struct Client clientList ;
	provServer.sockID = socket ( PF_INET , SOCK_STREAM , 0 ) ;
	provServer.myServer.sin_family = AF_INET ;
	provServer.myServer.sin_port = htons ( provServer.myPort ) ;
	provServer.myServer.sin_addr.s_addr = inet_addr ( provServer.myIP ) ;
	provServer.bindID = bind ( provServer.sockID , ( struct sockaddr* ) &provServer.myServer , sizeof ( struct sockaddr ) ) ;
	if ( provServer.bindID < 0 )
	{
		printf ( "ERROR. Could not bind server to the provided address. Please change the IP and try again.\n" ) ;
		return 0 ;
	}
	listen ( provServer.sockID , 12 ) ;
	while ( 1 )
	{
		int size = sizeof ( struct sockaddr_in ) ;
		clientList.masterPort = provServer.myPort ;
		clientList.clientID = accept ( provServer.sockID , ( struct sockaddr* ) &clientList.clientAddr , ( unsigned* ) &size  ) ;
		while ( ( clientList.clientThreadID = pthread_create ( &clientList.clientThread , NULL , voteThread , ( void* ) &clientList ) ) ) ;
	}
}

void* voteThread ( void* client )
{
	struct Client newClient = * ( ( struct Client* ) client ) ;
	recv ( newClient.clientID , newClient.msg , message_size , 0 ) ;
	char newMessage [ message_size ] ;
	memset ( newMessage , '\0' , message_size ) ;

	if ( newClient.masterPort == PORTB )
	{
		strcpy ( newMessage , "B" ) ;
		if ( strcmp ( newClient.msg , "TL" ) == 0 )
		{
			voteCountPTI [ 0 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "TU" ) == 0 )
		{
			voteCountPTI [ 0 ] [ 1 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NL" ) == 0 )
		{
			voteCountPMLN [ 0 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NU" ) == 0 )
		{
			voteCountPMLN [ 0 ] [ 1 ] += 1 ;
		}

	}
	else if ( newClient.masterPort == PORTK )
	{
		strcpy ( newMessage , "K" ) ;
		if ( strcmp ( newClient.msg , "TL" ) == 0 )
		{
			voteCountPTI [ 1 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "TU" ) == 0 )
		{
			voteCountPTI [ 1 ] [ 1 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NL" ) == 0 )
		{
			voteCountPMLN [ 1 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NU" ) == 0 )
		{
			voteCountPMLN [ 1 ] [ 1 ] += 1 ;
		}
	}
	else if ( newClient.masterPort == PORTP )
	{
		strcpy ( newMessage , "P" ) ;
		if ( strcmp ( newClient.msg , "TL" ) == 0 )
		{
			voteCountPTI [ 2 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "TU" ) == 0 )
		{
			voteCountPTI [ 2 ] [ 1 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NL" ) == 0 )
		{
			voteCountPMLN [ 2 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NU" ) == 0 )
		{
			voteCountPMLN [ 2 ] [ 1 ] += 1 ;
		}
	}
	else if ( newClient.masterPort == PORTS )
	{
		strcpy ( newMessage , "S" ) ;
		if ( strcmp ( newClient.msg , "TL" ) == 0 )
		{
			voteCountPTI [ 3 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "TU" ) == 0 )
		{
			voteCountPTI [ 3 ] [ 1 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NL" ) == 0 )
		{
			voteCountPMLN [ 3 ] [ 0 ] += 1 ;
		}
		else if ( strcmp ( newClient.msg , "NU" ) == 0 )
		{
			voteCountPMLN [ 3 ] [ 1 ] += 1 ;
		}
	}
	strcat ( newMessage , newClient.msg ) ;
	strcpy ( newClient.msg , newMessage ) ;
	//CONNECTING WITH THE SERVER
	int fedSockID ;
	int fedConnectionID ;
	struct sockaddr_in fedServerAddr ;

	fedSockID = socket ( PF_INET , SOCK_STREAM , 0 ) ;
	if ( fedSockID < 0 )
	{
		printf ( "ERROR. Could not setup SOCKET. The program will now exit\n" ) ;
		return 0 ;
	}
	fedServerAddr.sin_family = AF_INET ;
	fedServerAddr.sin_port = htons ( PORTFED ) ;
	fedServerAddr.sin_addr.s_addr = inet_addr ( FED ) ;

	while ( 1 )
	{
		fedConnectionID = connect ( fedSockID , ( struct sockaddr* ) &fedServerAddr , sizeof ( struct sockaddr ) ) ;
		if ( fedConnectionID < 0 )
		{
			printf ( "ERROR. Could not establish connection to the server. Retrying in 2 seconds.\n" ) ;
			sleep ( 2 ) ;
			continue ;
		}
		break ;
	}

	send ( fedSockID , newClient.msg , message_size , 0 ) ;
	memset ( newClient.msg , '\0' , message_size ) ;
	recv ( fedSockID , newClient.msg , message_size , 0 ) ;
	if ( ! ( strcmp ( newClient.msg , "CASTED" ) == 0 ) )
	{
		printf ( "ERROR. Vote NOT casted. Please try again later\n" ) ;
	}

	//INFORMING THE CLIENT THAT VOTE HAS BEEN CASTED
	send ( newClient.clientID , newClient.msg , message_size , 0 ) ;
	pthread_exit ( NULL ) ;
}
